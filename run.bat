@echo off

set "processId=0000"

netstat -o -n -a | find "LISTENING" | find "9999 " > NUL
if "%ERRORLEVEL%" equ "0" (
  echo Port 9999 is not empty
  for /f "tokens=5" %%a in ('netstat -aon ^| findstr 9999') do  set processId= %%a
  goto :askForPermission
) ELSE (
  goto :startApplication
)

:askForPermission
  set /p killProcess="Do you want to kill the process?(y/n)  "
  if /I "%killProcess%"=="y" goto :taskkill
  if /I "%killProcess%"=="n" goto :eof

:taskkill
  echo Initaiting task kill for processId - %processId%
  Taskkill /PID %processId% /F


:startApplication
  echo "----------  Starting the application ------------"
  java -jar SpringBootStarter.jar

pause