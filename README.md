# Sample Spring Boot starter application #

**Using** 

1. Java 7
2. Maven 3.3.9
3. Spring boot 1.3.5


**Build**

To build just run *package.bat*

**Execute**

To execute just run *run.bat*